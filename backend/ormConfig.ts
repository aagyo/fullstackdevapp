import { ConnectionOptions } from "typeorm";
import 'dotenv/config';

export const ormConfig: ConnectionOptions = {
    type: "postgres",
    host: "localhost",
    port: 9500,
    username:  "postgres" ,
    password: "postgres",
    database: "fsd",
    entities: ["./models/*.ts"],
    logging: false,
    synchronize: true
}