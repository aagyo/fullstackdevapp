import jwt from "jsonwebtoken";
import { jwtToken } from "./jwtToken";
import { NextFunction, Request, Response } from "express";
import router from "routes/userRoutes";

 export function checkJwt(req : Request, res : Response,  next : NextFunction) : void {
     try{
        let token = req.headers?.authorization?.split('Bearer ')[1]; 
        jwt.verify(token, jwtToken.secret);
        next();
        return;
        }catch(err){
            console.log("ERR");
            res.status(401).send();
            return;

        }
    }



