const PNGReader = require("./PNGReader");

export function toArrayBuffer(buff : Buffer) : ArrayBuffer{
    var ab = new ArrayBuffer(buff.length);
    var view = new Uint8Array(ab);
    for(var i = 0; i < buff.length; ++i){
        view[i] = buff[i];
    }
    return ab;
}



function getDataFromReader(buff : any){
    return new Promise((resolve, reject) => {
        const reader = new PNGReader(buff);
        reader.parse( async(err : any, png : any) => {
            console.log(png.pixels);

          const pixels = Float32Array.from(png.pixels).map(pixel => {
              return pixel / 255;
          })
          
          if(pixels != undefined)
            resolve(pixels);
        
        reject();
          
        });
    })
}

export async function toImage(buff : ArrayBuffer) {
    try{
        let values : any;
        await getDataFromReader(buff)
            .then((pixels) => values = pixels)
            .catch((err) => console.log(err));
        return values;
    }catch(error) {
        console.log(error);
    }
 
      
}