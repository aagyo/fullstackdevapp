import express, { json } from "express";
import * as tf from '@tensorflow/tfjs-node'
import {MnistData} from '../utils/data';
import { toArrayBuffer, toImage} from '../utils/converter';
import { any, imag, mul, Tensor } from "@tensorflow/tfjs-node";
import { checkJwt } from "../utils/jwtChecker";
import { jwtToken } from "utils/jwtToken";
import jwt from "jsonwebtoken";
import { getManager, getRepository } from "typeorm";
import { User } from "../models/user";
import { Table } from "../models/table";
var multer = require('multer');
const router = express.Router();
const IMAGE_WIDTH = 28;
const IMAGE_HEIGHT = 28;
const testDataSize = 1;


router.post('/api/upload', [checkJwt],async function(req : any, res : any) {
  try{

    const image = await req.files.image;
    const imageData : Buffer = image.data;

    
    // console.log(imageData);
    
    // const bufferArray = imageData.buffer.slice(imageData.byteOffset, imageData.byteOffset + imageData.byteLength);
    // let imageDataAsArrayBuffer = toArrayBuffer(imageData);
    // var values = await toImage(bufferArray);
    // console.log(values);
    // const firstChanel = values.slice(0, 784);
    // const secondChanel = values.slice(784, 784 * 2);
    // const thirdChanel = values.slice(784 * 2, 784 * 3);

    // console.log(firstChanel);
    // console.log(secondChanel);
    // console.log(thirdChanel);

    // const xs = tf.tensor2d(values.slice(0,784), [1, 784]);
    // const passedXs = xs.reshape([testDataSize, IMAGE_WIDTH, IMAGE_HEIGHT, 1]);
    var decodedImage = tf.node.decodeImage(imageData);
    // console.log("DECODED IMAGE:");
    // console.log(decodedImage);

    let rFactor = tf.scalar(0.2989);
    let gFactor = tf.scalar(0.5870);
    let bFactor = tf.scalar(0.1140);

  // separate out each channel. x.shape[0] and x.shape[1] will give you
  // the correct dimensions regardless of image size
    let r = decodedImage.slice([0,0,0], [decodedImage.shape[0], decodedImage.shape[1], 1]);
    let g = decodedImage.slice([0,0,1], [decodedImage.shape[0], decodedImage.shape[1], 1]);
    let b = decodedImage.slice([0,0,2], [decodedImage.shape[0], decodedImage.shape[1], 1]);

   // add all the tensors together, as they should all be the same dimensions.
    let grayImage = r.mul(rFactor).add(g.mul(gFactor)).add(b.mul(bFactor)).expandDims(0);
  
    

    const model = await tf.loadLayersModel('file://../backend/mlModel/model.json');

    const preds : any = model.predict(grayImage);
    grayImage.dispose();
    decodedImage.dispose();
    console.log("--------");
    console.log(preds.toString());
    await preds.data()
      .then(async (data : any) =>  
      {
        
        await createTableEntry(req.headers?.authorization?.split('Bearer ')[1], image.name, indexOfMax(data).toString(), image.size);
        res.status(200).send(indexOfMax(data).toString());
      });

  }catch(err){
    console.log(err);
  }
});

async function  createTableEntry(token : string, fileName :string, result : string, size : number) : Promise<void>{
  let decodedToken : any = jwt.decode(token);
  console.log("TOQUEN");
  let userEmail = decodedToken["email"];
  console.log(userEmail);
  let repository = getRepository(User);
  await repository.findOneOrFail( {email : userEmail }, {relations: ["tables"]}).then(async (user) => {
    console.log(user);
    let table = new Table(fileName, result, user, size);
    
    let manager = getManager();
    // await manager.save(table);
    user.tables.push(table);
    await manager.save(user);
  }
  ).catch((err) => console.log(err))
}

function indexOfMax(arr : any) : number {
  if (arr.length === 0) {
      return -1;
  }

  var max = arr[0];
  var maxIndex = 0;

  for (var i = 1; i < arr.length; i++) {
      if (arr[i] > max) {
          maxIndex = i;
          max = arr[i];
      }
  }

  return maxIndex;
}

router.get('/api/evaluate', async (req, res) =>{
    
    try{
      
        const data = new MnistData();
        await data.load();
        const model = await tf.loadLayersModel('file://../backend/mlModel/model.json');
        const testData = data.nextTestBatch(testDataSize);
        const testxs = testData.xs.reshape([testDataSize, IMAGE_WIDTH, IMAGE_HEIGHT, 1]);
        const labels = testData.labels.argMax(-1);
        const preds = model.predict(testxs);
        testxs.dispose();
        res.status(200).send(preds + "\n" + labels);

    }catch(err){
        console.log(err);
    }
      
    

});


router.get("/api/train",async(req,res) =>{
    const data = new MnistData();
    await data.load();
    const model = getModel();
    await train(model, data);
    model.save('file://../backend/mlModel/model.json')
    res.send(200);
})

async function train(model : any, data : any) {
    const metrics = ['loss', 'val_loss', 'acc', 'val_acc'];
    const container = {
      name: 'Model Training', tab: 'Model', styles: { height: '1000px' }
    };
    
    function onBatchEnd(batch : any, logs : any) {
        console.log('Accuracy', logs.acc);
      }
    
    const BATCH_SIZE = 1024;
    const TRAIN_DATA_SIZE = 5500;
    const TEST_DATA_SIZE = 1000;
  
    const [trainXs, trainYs] = tf.tidy(() => {
      const d = data.nextTrainBatch(TRAIN_DATA_SIZE);
      return [
        d.xs.reshape([TRAIN_DATA_SIZE, 28, 28, 1]),
        d.labels
      ];
    });
  
    const [testXs, testYs] = tf.tidy(() => {
      const d = data.nextTestBatch(TEST_DATA_SIZE);
      return [
        d.xs.reshape([TEST_DATA_SIZE, 28, 28, 1]),
        d.labels
      ];
    });
  
    return model.fit(trainXs, trainYs, {
      batchSize: BATCH_SIZE,
      validationData: [testXs, testYs],
      epochs: 25,
      shuffle: true,
      callbacks: {onBatchEnd}
    });
  }

  function getModel() {
    const model = tf.sequential();
    
    const IMAGE_WIDTH = 28;
    const IMAGE_HEIGHT = 28;
    const IMAGE_CHANNELS = 1;  
    
    // In the first layer of our convolutional neural network we have 
    // to specify the input shape. Then we specify some parameters for 
    // the convolution operation that takes place in this layer.
    model.add(tf.layers.conv2d({
      inputShape: [IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS],
      kernelSize: 5,
      filters: 8,
      strides: 1,
      activation: 'relu',
      kernelInitializer: 'varianceScaling'
    }));
  
    // The MaxPooling layer acts as a sort of downsampling using max values
    // in a region instead of averaging.  
    model.add(tf.layers.maxPooling2d({poolSize: [2, 2], strides: [2, 2]}));
    
    // Repeat another conv2d + maxPooling stack. 
    // Note that we have more filters in the convolution.
    model.add(tf.layers.conv2d({
      kernelSize: 5,
      filters: 16,
      strides: 1,
      activation: 'relu',
      kernelInitializer: 'varianceScaling'
    }));
    model.add(tf.layers.maxPooling2d({poolSize: [2, 2], strides: [2, 2]}));
    
    // Now we flatten the output from the 2D filters into a 1D vector to prepare
    // it for input into our last layer. This is common practice when feeding
    // higher dimensional data to a final classification output layer.
    model.add(tf.layers.flatten());
  
    // Our last layer is a dense layer which has 10 output units, one for each
    // output class (i.e. 0, 1, 2, 3, 4, 5, 6, 7, 8, 9).
    const NUM_OUTPUT_CLASSES = 10;
    model.add(tf.layers.dense({
      units: NUM_OUTPUT_CLASSES,
      kernelInitializer: 'varianceScaling',
      activation: 'softmax'
    }));
  
    
    // Choose an optimizer, loss function and accuracy metric,
    // then compile and return the model
    const optimizer = tf.train.adam();
    model.compile({
      optimizer: optimizer,
      loss: 'categoricalCrossentropy',
      metrics: ['accuracy'],
    });
  
    return model;
  }
  
  

export default router;
