import express, { Router } from "express";
import { getManager, getRepository } from "typeorm";
import { checkJwt } from "../utils/jwtChecker";
import {Table} from "../models/table"
import jwt from "jsonwebtoken";
import { User } from "../models/user";

const router = express.Router();

// router.use(checkJwt);

function getUserFromRequest( req : any) : string{
  let token = req.headers?.authorization?.split('Bearer ')[1]; 
  console.log(token);
  let decodedToken : any = jwt.decode(token);
  return decodedToken['email'];
}

router.get('/api/table', [checkJwt] , async(req  : any , res : any ) =>{
 
  // console.log(token);
  let userEmail = getUserFromRequest(req);
  var repository = getRepository(User);
  var user : User | void = await repository.findOneOrFail({email : userEmail}, {relations: ["tables"]})
              .catch((err) => console.log(err));
    if(typeof(user) == "object")
    {
      res.send(user.tables);

    }else{
      res.sendStatus(400);
    }

  });

  router.post('/api/table',[checkJwt] , async(req : any , res : any) =>{
    const tableRepository = getRepository(Table);
    tableRepository.save(req.body).then(() => {
      res.send(201);
    }).catch(() => {
      res.send(400);
    });
  });

  export default router;