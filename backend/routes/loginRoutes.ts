import express from "express";
import { User } from "../models/user";
import { getRepository } from "typeorm";
import { jwtToken } from "../utils/jwtToken";

const router = express.Router();



router.post('/api/login',  async (request , response ) => {
    const userRepository = getRepository(User);
    await userRepository
      .findOneOrFail({ where: { email: request.body.email } })
      .then(async (user) => {
        const tokenData = jwtToken.createToken(user);
        // response.setHeader("Set-Cookie", [jwtToken.createCookie(tokenData)]);
        // response.cookie("auth", tokenData);
        return response.status(200).json(tokenData);
      })
      .catch((err) => {
          console.log(err);
          return response.sendStatus(401);
      });
  })

export default router;