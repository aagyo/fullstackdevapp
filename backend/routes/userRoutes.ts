import express from "express";
import { User } from "../models/user";
import { getManager, getRepository } from "typeorm";
import { checkJwt } from "../utils/jwtChecker";
import { send } from "node:process";

const router = express.Router();


router.get('/api/users', [checkJwt],  async (req : any  ,res : any ) => 
{
    
    const entityManager = getManager();
    var users = await entityManager.find(User);
    res.send(users);
  });

router.post('/api/users',[checkJwt] , async (req : any, res : any) =>{
    const userRepository = getRepository(User);
    userRepository.save(req.body)
    .then(() => {
      res.status(201).send()
    }).catch(() => {
      res.status(400).send();
    });
    
  })

router.get('/api/allUsers', async (req : any, res : any) =>{
  try{
    const userManager = getRepository(User);
    var users = await userManager.find({relations: ["tables"]});
    res.send(users);
  }catch(err){
    console.log(err);
    res.sendStatus(401);
  }
  
})

export default router;