import "reflect-metadata";
import "dotenv/config";
import express from "express";
import { ormConfig } from "./ormConfig";
import { createConnection, getManager, getRepository, RepositoryNotTreeError } from "typeorm";
import { NextFunction, Request, Response } from "express";
import userRoutes from "./routes/userRoutes";
import loginRoutes from "./routes/loginRoutes"
import tableRoutes from "./routes/tableRoutes"
import evaluateRoutes from "./routes/evaluateRoutes";


const app = express();
const morgan = require('morgan');
const port = 3000;
const cors = require('cors')
const fileUpload = require('express-fileupload');

async function init() {

  let retries = 2
  while(retries) {
    try {
      await createConnection(ormConfig);
      break;
    } catch(err) {
      console.log(err);
      console.log('retires left: ', retries);
      retries -= 1;
    }
  }

  app.use(express.json());
  app.use(cors());
  app.use(morgan('dev'));
  app.use(fileUpload({
    useTempFiles : false,
    tempFileDir : '/tmp/'
  }));


  app.use(loginRoutes);
  app.use(userRoutes);
  app.use(tableRoutes);
  app.use(evaluateRoutes);

  app.use( (err : Error, req : Request, res : Response, next : NextFunction) => {
    console.log(err);
    res.status(500).send("Internal Server Error!");
  });

 

  await app.listen(port);
}


init().then(() => {
  console.log(`Server is listening to port: ${port}`);
})
