import {
    Column,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
    
  } from "typeorm";
  import {Table} from './table';
  @Entity()
  export class User {
    @PrimaryGeneratedColumn()
    id!: number;
  
    @Column({ length: 64, unique: true })
    email!: string;
  
    @Column({ length: 64 })
    password!: string;

    @OneToMany(() => Table, table => table.user, {
      cascade: true
    })
  
    tables : Table[];

  }