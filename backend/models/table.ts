import {
    Column,
    Entity,
    Generated,
    ManyToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn,
  } from "typeorm";
import { User } from "./user";
  
  @Entity()
  export class Table {
    @PrimaryColumn()
    @Generated("uuid")
    id!: string;
  
    @Column({ length: 64 })
    Name!: string;
  
    @Column({})
    Size!: number;

    @Column({ length: 64 })
    Result!: string;

    @ManyToOne(() => User, user => user.tables)
    user: User;

    constructor( name : string, result : string, user : User, size : number){
        this.Name = name;
        this.Result = result;
        this.user = user;
        this.Size = size;
    }
  }