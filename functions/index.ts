const {Datastore} = require('@google-cloud/datastore');

const dataStore = new Datastore({
  projectId: 'careful-sphinx-310614',
  keyFilename: 'creditentials.json'
})

const cors = require('cors')({origin: true});

export async function helloWorld(req : any, res : any) {
  cors(req, res, async () => {
    const query = dataStore.createQuery('user');

    let [users] = await dataStore.runQuery(query)

    for (const user of users) {
      const userKey = user[dataStore.KEY];
      console.log(userKey.id, user);
    }

    res.send(users);
  })
  
  };

