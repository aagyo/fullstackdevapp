import {EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  events : boolean;
  //events = new EventEmitter<boolean>();

  constructor() { this.events = false }

  show() {
     this.events = true;
  }

  hide() {
    this.events = false;
  }
}
