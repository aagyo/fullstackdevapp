import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestService } from './request.service';
import { map } from 'rxjs/operators';
import { ReactiveFormsModule } from '@angular/forms';
@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  private readonly endPoint : string = "upload";

  constructor(private requestService : RequestService) { }


  postFile(fileToUpload : any) : boolean{ 
      if(fileToUpload == undefined)
      {
        alert("Please upload a file!");
        return false;
      }
      
      const formData : FormData = new FormData();
      formData.append('image', fileToUpload, fileToUpload.name);
      this.requestService.post(this.endPoint,formData).subscribe((data) => alert(`Your answer is: ${data}`));
      return false;
  }
}
