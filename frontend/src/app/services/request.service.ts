import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  constructor(private http : HttpClient) { }

  baseUrl : string = "http://localhost:3000/api/"

  post(url : string, body : any) : Observable<any> {
    const customHeaders = {
      headers: new HttpHeaders({
        'content-type': 'application/json'
      })
    };
    const headers = new HttpHeaders({'Authorization': `Bearer ${localStorage.getItem('authToken')}`});
    return this.http.post(this.baseUrl + url, body, {headers});
  }

  get(url : string) : Observable<any>{
    const headers = new HttpHeaders({'Content-Type' : 'application/json','Authorization': `Bearer ${localStorage.getItem('authToken')}`});
    return this.http.get(this.baseUrl + url, {headers});
  }
}
