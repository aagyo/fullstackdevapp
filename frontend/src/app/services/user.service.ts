import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import {map, tap} from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  public userData : any;

  constructor(private requestService : RequestService) { }

  login(body : any) : Observable<any> {
    return this.requestService.post("login", body).pipe(
      tap(data => {
        console.log(data);
        this.userData = data;
      })
    )
  }
}
