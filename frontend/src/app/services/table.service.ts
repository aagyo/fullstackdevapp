import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import {Table} from "../entities/table"
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TableService {

  private tablesRequestUrl : string = 'table'

  constructor(private requestService : RequestService) { 
  }

  public getTables() : Observable<Table[]>{
    return this.requestService.get(this.tablesRequestUrl);
  }
}
