import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CloudComponent } from './cloud/cloud.component';
import { ImageComponent } from './image/image.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { TableComponent } from './table/table.component';

const routes: Routes = [{path: '', redirectTo: 'login', pathMatch: 'full'},
{path: 'table', component: TableComponent, canActivate: [AuthGuardService]},
{path: 'login', component: LoginComponent},
{path: 'image', component: ImageComponent, canActivate: [AuthGuardService]},
{path: 'cloud', component: CloudComponent} ];

@NgModule({
  imports: [RouterModule.forRoot( routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
