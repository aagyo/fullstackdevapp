import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RequestService } from '../services/request.service';

@Component({
  selector: 'app-cloud',
  templateUrl: './cloud.component.html',
  styleUrls: ['./cloud.component.scss']
})
export class CloudComponent implements OnInit {
  public Users! : string;

  constructor(private requestService : RequestService, private http : HttpClient) { }

  ngOnInit(): void {
    this.getUsers().subscribe((data) =>{
      this.Users = JSON.stringify(data);
      console.log(data);
    } );
  }

  public getUsers() : Observable<any> {
    return this.http.get("https://us-central1-careful-sphinx-310614.cloudfunctions.net/getUsers");
  }

}
