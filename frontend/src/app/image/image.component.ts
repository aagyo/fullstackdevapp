import { Component, OnInit } from '@angular/core';
import { FileUploadService } from '../services/file-upload.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
  public imagePath: any;
  imgURL: any;
  public message: string = '';
  private fileToUpload : any;

  preview(files: any) {
    console.log(files.item(0));
    if (files.length === 0)
      return;

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }

    var reader = new FileReader();
    this.imagePath = files;
    console.log(this.imagePath);

    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;

    }
    this.fileToUpload = files.item(0);
  }

  public sendFile() : void{
    console.log(this.fileUploadService.postFile(this.fileToUpload));
  }

  openFile() {
    document.querySelector("input")?.click()
  }

  constructor(private fileUploadService : FileUploadService) {
   
   }

  ngOnInit(): void {
  }

}
