import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HeaderService} from '../services/header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public header : HeaderService, private router : Router) { }

  ngOnInit(): void {
    this.header.show();
  }

  logoutButton() : void{
    localStorage.clear();
    this.router.navigate(['login']);
    console.log('loggout');
  }
}
