import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { HeaderService } from '../services/header.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(4)]),
  });



  constructor(private userService : UserService, private router : Router
    ,private headerService : HeaderService) { }

  avatar : string = "assets/images/user.png";

  ngOnInit(): void {
    this.headerService.hide();
  }

  onSubmit() {
    this.userService.login(this.loginForm.getRawValue())
    .subscribe(data => {
      localStorage.setItem('authToken', data.token);
      this.router.navigate(['table']);
      this.headerService.show();
    }, err => {
      alert(err.message)
    });
  }

}
